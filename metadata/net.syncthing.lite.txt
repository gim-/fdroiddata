Categories:Internet
License:MPL-2.0
Web Site:https://syncthing.net
Source Code:https://github.com/Nutomic/syncthing-lite
Issue Tracker:https://github.com/Nutomic/syncthing-lite/issues
Changelog:https://github.com/Nutomic/syncthing-lite/releases

Auto Name:Syncthing Lite
Summary:File synchronization
Description:
This project is an Android app, that works as a client for a Syncthing share
(accessing Syncthing devices in the same way a client-server file sharing app
ccess its proprietary server).

This is a client-oriented implementation, designed to work online by downloading
and uploading files from an active device on the network (instead of
synchronizing a local copy of the entire repository). This is quite different
from the way the syncthing-android works, and its useful from those devices that
cannot or wish not to download the entire repository (for example, mobile
devices with limited storage available, wishing to access a syncthing share).
.

Repo Type:git
Repo:https://github.com/Nutomic/syncthing-lite.git

Build:0.1.5,7
    commit=0.1.5
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags ^[0-9.]*$
Current Version:0.1.5
Current Version Code:7
